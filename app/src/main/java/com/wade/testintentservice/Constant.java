package com.wade.testintentservice;

/**
 * Created by wade on 2016/9/23.
 */

public class Constant {
    public static final String SERVICE_NAME = "intent service";
    public static final String FLAG_TASK = "task";
    public static final String FLAG_RECEIVER = "receiver";
    public static final int TASK1 = 1;
    public static final int TASK2 = 2;
    public static final int RESULT_CODE = 100;
    public static final String RETURN_TASK = "return_task";
    public static final String TASK1_MSG = "task 1...";
    public static final String TASK2_MSG = "task 2...";
}
