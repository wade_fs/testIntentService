package com.wade.testintentservice;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final String TAG="myService";
    private Button task1;
    private Button task2;
    private TextView result;
    private MyBroadcastReceiver receiver;
    private StringBuffer stringBuffer;

    public class MyBroadcastReceiver extends BroadcastReceiver{
        public static final String PROCESS_RESPONSE = "PROCESS_RESPONSE";

        @Override
        public void onReceive(Context context, Intent intent) {
            String returnStr = intent.getStringExtra(Constant.RETURN_TASK);

            if(returnStr != null) {
                stringBuffer.append(returnStr + "\n");
                result.setText(stringBuffer);
                Log.i(TAG, "Activity.onReceiveResult("+returnStr+")");
            }
            else
                Log.i(TAG, "Activity.onReceiveResult(null)");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "Activity.onCreate("+(savedInstanceState == null)+")");
        stringBuffer = new StringBuffer();
        IntentFilter filter = new IntentFilter(MyBroadcastReceiver.PROCESS_RESPONSE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new MyBroadcastReceiver();
        Log.i(TAG, "Activity.initData(), will registerReceiver!");
        registerReceiver(receiver, filter);
        task1 = (Button) findViewById(R.id.send_task1);
        task2 = (Button) findViewById(R.id.send_task2);
        result = (TextView) findViewById(R.id.result);
        task1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Activity.onClick(task1)");
                Intent intent = new Intent(MainActivity.this, MyIntentService.class);
                intent.putExtra(Constant.FLAG_TASK, Constant.TASK1);
                startService(intent);
            }
        });
        task2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Activity.onClick(task2)");
                Intent intent = new Intent(MainActivity.this, MyIntentService.class);
                intent.putExtra(Constant.FLAG_TASK, Constant.TASK2);
                startService(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Activity.onDestroy(), will unregisterReceiver!");
        unregisterReceiver(receiver);
        super.onDestroy();
    }
}