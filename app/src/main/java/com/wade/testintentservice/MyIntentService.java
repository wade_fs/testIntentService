package com.wade.testintentservice;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by wade on 2016/9/21.
 */

public class MyIntentService extends IntentService {
    private final String TAG="myService";
    public MyIntentService() {
        super(Constant.SERVICE_NAME);
        Log.i(TAG, "Service.MyIntentService: " + Constant.SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int taskFlag = intent.getIntExtra(Constant.FLAG_TASK, 1);
        BroadcastReceiver receiver = intent.getParcelableExtra(Constant.FLAG_RECEIVER);
        int rand = (int)(Math.random()*5 + 1);
        try {
            Thread.sleep(rand * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(taskFlag == 1){
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(MainActivity.MyBroadcastReceiver.PROCESS_RESPONSE);
            broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
            broadcastIntent.putExtra(Constant.RETURN_TASK, Constant.TASK1_MSG);
            Log.i(TAG, "Service.onHandleIntent("+taskFlag+","+Constant.TASK1_MSG+")");
            sendBroadcast(broadcastIntent);
        } else{
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(MainActivity.MyBroadcastReceiver.PROCESS_RESPONSE);
            broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
            broadcastIntent.putExtra(Constant.RETURN_TASK, Constant.TASK2_MSG);
            Log.i(TAG, "Service.onHandleIntent("+taskFlag+","+Constant.TASK2_MSG+")");
            sendBroadcast(broadcastIntent);
        }
    }
}
